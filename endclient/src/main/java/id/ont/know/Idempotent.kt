package id.ont.know

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import id.ont.know.model.SearchableItem
import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.logging.Logger


var killswitch=false

/**
 * gson singleton -- the memory leak was fixed a few years back
 */
val gson: Gson by lazy { GsonBuilder().setPrettyPrinting().create() }
/*
        it can return the list of pages selected and indexed,
*/
val searchable by lazy {
    Executors.newCachedThreadPool().invokeAll(List<Callable<SearchableItem>>(10) {
        Callable {
            randomSearchableContent()
        }
    }).map { it.get(3, TimeUnit.MINUTES) }
}

infix fun <A, B> Pair<A, B>.by(third: B): Triple<A, B, B> = Triple(first, second, third)
val log = Logger.getAnonymousLogger() //already lazy/cached
val stopwords by   lazy { Files.readAllLines(Paths.get("src/main/resources/stopwords.txt")).toSortedSet() }