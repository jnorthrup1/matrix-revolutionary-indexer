package id.ont.know.context

import com.sun.net.httpserver.HttpExchange
import id.ont.know.apiResponse
import id.ont.know.gson
import id.ont.know.model.QueryRanking
import id.ont.know.searchable

val searchContext: (exchange: HttpExchange) -> Unit = { exchange ->
    val query = exchange.requestURI.query.split("q=").last()
    val ranking = searchable.map {
        val occurences = it.weights.getOrDefault(query, 0)
        val domainFraction = occurences.toDouble() / it.termsIndexed.toDouble()
        QueryRanking(it.topic, it.link, occurences.toDouble(), domainFraction)
    }.sortedBy { it.score * it.domainSpecificity }.asReversed()
    val res =
            gson.toJson(listOf(query, ranking))
    apiResponse(exchange, res)

}