package id.ont.know.context

import com.sun.net.httpserver.HttpExchange
import id.ont.know.apiResponse
import id.ont.know.gson
import id.ont.know.searchable

val listContext: (exchange: HttpExchange) -> Unit = { exchange ->

    apiResponse(exchange, gson.toJson(searchable.map {
        it.topic to it.link

    }.toMap()
    ))

    /**
     * smoke tested
     * {
    "Mierzwin_Du%C5%BCy": "https://en.wikipedia.org/api/rest_v1/page/html/Mierzwin_Du%C5%BCy",
    "Chignik%2C_Alaska": "https://en.wikipedia.org/api/rest_v1/page/html/Chignik%2C_Alaska",
    "Bluetongue_disease": "https://en.wikipedia.org/api/rest_v1/page/html/Bluetongue_disease",
    "Ann_Jones_(author)": "https://en.wikipedia.org/api/rest_v1/page/html/Ann_Jones_(author)",
    "Jos%C3%A9_Borello": "https://en.wikipedia.org/api/rest_v1/page/html/Jos%C3%A9_Borello",
    "9th_Ward_of_New_Orleans": "https://en.wikipedia.org/api/rest_v1/page/html/9th_Ward_of_New_Orleans",
    "Irrawaddy_dolphin": "https://en.wikipedia.org/api/rest_v1/page/html/Irrawaddy_dolphin",
    "Io_Murota": "https://en.wikipedia.org/api/rest_v1/page/html/Io_Murota",
    "Echo_(mythology)": "https://en.wikipedia.org/api/rest_v1/page/html/Echo_(mythology)",
    "K%C3%B6nigsbr%C3%BCck_railway_station": "https://en.wikipedia.org/api/rest_v1/page/html/K%C3%B6nigsbr%C3%BCck_railway_station"
    }
     */
}