package id.ont.know

import com.kohlschutter.boilerpipe.extractors.ArticleExtractor
import id.ont.know.model.SearchableItem
import java.io.InputStreamReader
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths


fun createIndex(headerFields: MutableMap<String, MutableList<String>>, content: String?): SearchableItem {

    val link = headerFields["content-location"]!!.first()
    val topic = link.split("/").last()
    val location = "/tmp/wikiprime:${link.replace("/", "@@")}.txt"

    log.fine("writing to $location")

    /**
     * a list of topic,link, content, searchable in the index.
     */
    val medium = listOf(topic, link, content).joinToString("\n")
    val keys = (medium.split("\\W+".toRegex()).map(String::toLowerCase).filter { it.length > 2 }) - stopwords
    val weights = sortedMapOf<String, Int>()
    for (k in keys)
        weights[k] = when {
            k in weights -> weights.get(k)!!.toInt() + 1
            else -> 1
        }
    val usefulTerms = weights.values.sum()
    val searchableItem = SearchableItem(link, topic, location, medium, weights, usefulTerms)

    return searchableItem.also { Files.writeString(Paths.get(location), gson.toJson(searchableItem)) }
}

fun randomSearchableContent(): SearchableItem {
    val url1 = URL("https://en.wikipedia.org/api/rest_v1/page/random/html")
    val openConnection = url1.openConnection()
    val headerFields = openConnection.headerFields
    val content = ArticleExtractor.INSTANCE.getText(InputStreamReader(openConnection.getInputStream()))
    log.finest(content)

    return createIndex(headerFields, content)
}