package id.ont.know.model


import java.util.*

data class SearchableItem(val link: String,
                          val topic: String,
                          val location: String,

                          /**
                           * the indexed flat text:
                           * topic EOL
                           * link EOL
                           * article
                           */
                          val medium: String,
                          val weights: SortedMap<String, Int>,
                          val termsIndexed: Int
) {
    /*
        it can return the number of terms indexed, and ideally a list of the top n terms, for each page,
    */
    val topN get() = weights.entries.sortedWith(ranker)

    companion object {
        object ranker : Comparator<Map.Entry<String, Int>> {
            override fun compare(o1: Map.Entry<String, Int>, o2: Map.Entry<String, Int>): Int {
                var res: Int = o2.value.compareTo(o1.value)
                if (res == 0)
                    res = o1.key.compareTo(o2.key)

                return res
            }
        }
    }
}