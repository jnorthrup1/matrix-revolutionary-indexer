package id.ont.know.context

import com.sun.net.httpserver.HttpExchange
import id.ont.know.apiResponse
import id.ont.know.gson
import id.ont.know.model.IndexStats
import id.ont.know.searchable

/*
        it can return the number of terms indexed, and ideally a list of the top n terms, for each page,
*/
val statsContext: (exchange: HttpExchange) -> Unit = {

    val distinct = searchable.map { it.weights.values.size }.sum()

    val aggregate = searchable.map { it.weights.values.sum() }.sum()

    val summary = linkedMapOf<String, Int>()
    val statList = searchable.map {
        val top10 = it.topN
        top10.map { (k, v) -> summary[k] = summary[k]?.let { it + v } ?: v }
        it.topic to top10.map { it.key to it.value }.take(10).toMap()
    }.toMap()

    val global = linkedMapOf<Int, MutableSet<String>>()
    summary.entries.sortedByDescending(MutableMap.MutableEntry<String, Int>::value).forEach { (k, v) ->
        global[v]?.add(k) ?: Unit.also { global[v] = sortedSetOf(k) }
    }


    apiResponse(it, gson.toJson(IndexStats(distinct, aggregate, statList, global), IndexStats::class.java))
}