package id.ont.know.model


/**
 * semantic naming
 */
data class QueryRanking(
        val topic: String,
        val link: String,
        val score: Double,
        val domainSpecificity: Double
)