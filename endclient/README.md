
# hypothetical indexer api

##   GET     /list     
returns indexed wikipedia/blogger objects 
return json object: topic: link

##  GET   /stats   

return 

data class IndexStats(val distinct_terms: Int,
                      val aggregate_mappings: Int,
                      val histogram: Map<String, Map<String, Int>>,
                      val allCounts: LinkedHashMap<Int, MutableSet<String>>

)

json schema:

```
{
  "$schema": "http://json-schema.org/draft-07/schema",
  "$id": "http://example.com/example.json",
  "type": "object",
  "readOnly": false,
  "writeOnly": false,
  "minProperties": 0,
  "title": "/stats",
  "description": "indexer stats, global",
  "additionalProperties": true,
  "required": [
    "distinct_terms",
    "aggregate_mappings",
    "histogram",
    "allCounts"
  ],
  "properties": {
    "distinct_terms": {
      "$id": "#/properties/distinct_terms",
      "type": "integer",
      "readOnly": false,
      "writeOnly": false,
      "title": "distinct mapped keys found after stopwords",
      "description": "the numerical count",
      "default": 0,
      "examples": [
        2705
      ]
    },
    "aggregate_mappings": {
      "$id": "#/properties/aggergate_mappings",
      "type": "integer",
      "readOnly": false,
      "writeOnly": false,
      "title": "The Aggregate Mappings",
      "description": "count of all mapped index entries.",
      "default": 0,
      "examples": [
        4802
      ]
    },
    "histogram": {
      "$id": "#/properties/histogram",
      "type": "object",
      "readOnly": false,
      "writeOnly": false,
      "minProperties": 0,
      "title": "The Histogram Schema",
      "description": "histogram of keys and mapping counts, top-10, ",
      "default": {},  
      "additionalProperties": true 
    },
    "allCounts": {
      "$id": "#/properties/allCounts",
      "type": "object",
      "readOnly": false,
      "writeOnly": false,
      "minProperties": 0,
      "title": "The Allcounts Schema",
      "description": "An explanation about the purpose of this instance.",
      "default": {},
      "examples": [
        {
          "10": [
            "api",
            "army",
            "assembly",
            "century",
            "china",
            "colors",
            "elections",
            "forces",
            "history",
            "html",
            "https",
            "may",
            "member",
            "middle",
            "org",
            "page",
            "political",
            "region",
            "rest_v1",
            "three",
            "used",
            "various",
            "war",
            "wikipedia"
          ]         }
]}}}
```
##  GET  /s?q=...  

query param q

returns 
```
data class SearchableItem(val link: String,
                          val topic: String,
                          val location: String,

                          /**
                           * the indexed flat text:
                           * topic EOL
                           * link EOL
                           * article
                           */
                          val medium: String,
                          val weights: SortedMap<String, Int>,
                          val termsIndexed: Int
)
```

##  GET   /kill 

control proof of concept to manage serverice FSM.

