package id.ont.know

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpServer
import id.ont.know.context.listContext
import id.ont.know.context.searchContext
import id.ont.know.context.statsContext
import java.io.OutputStream
import java.net.InetSocketAddress

@OptIn(ExperimentalStdlibApi::class)
@Throws(Exception::class)
fun main() {

    /**
     * 10 considerate sequential pulls from wikipedia's randomness.  no bursty things with threads here.
     */
    try {
        assert(searchable.size == 10) { "did not fully fetch all results" }

        val server = HttpServer.create(InetSocketAddress(8000), 111)
        server.createContext("/list", listContext)
        server.createContext("/stats", statsContext)
        server.createContext("/s", searchContext)
        server.createContext("/kill", { killswitch=true /*handwavey jmx thing goes here. */ } )
        server.executor = null // creates a default executor
        server.start()
    } catch (e: Exception) {
        e.printStackTrace()
    }

    while (!killswitch) {
        Thread.sleep(10000)
    }
}

/**
 * returns a json string, also some client in the headers
 */
fun apiResponse(exchange: HttpExchange, res: String) {

    exchange.responseHeaders["content-type"] = "application/json"
    val remoteAddress = exchange.remoteAddress
    val chostname = "web client from ${remoteAddress.hostName}"
    exchange.responseHeaders["client-breadcrumbs"] = chostname
    exchange.sendResponseHeaders(200, res.length.toLong())
    val os: OutputStream = exchange.responseBody
    os.write(res.toByteArray())
    os.close()
    val requestURI = exchange.requestURI
    log.info("$requestURI sent to $remoteAddress/$chostname")
}