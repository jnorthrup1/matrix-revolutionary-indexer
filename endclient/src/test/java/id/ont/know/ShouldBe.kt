package id.ont.know

import org.junit.Assert

infix fun Any.shouldBe(expected: Any?) = Assert.assertEquals(expected, this)