package id.ont.know;




/**

  program should   expose a simple REST API that provides the following capabilities:


        given a search query, it will return the matching pages, in most relevant to least, including at least the
        following information:
it can return the list of pages selected and indexed,

 api: /list


it can return the number of terms indexed, and ideally a list of the top n terms, for each page,
 api:/stats


given a search query, it will return the matching pages, in most relevant to least, including at least the following information:
the matching articles, in order of relevance to the search, and
a "score" which gives an indication of relative relevance

 api: /s?q=TERM


expose additional information via your API that might be useful for someone who is trying to understand or break down your relevance/scoring mechanism i.e. for a given score x, what values were used in determining x, and how?

        All responses from the API should be in JSON format.

        When executed, your program should print to standard output at least the following items:


*/
