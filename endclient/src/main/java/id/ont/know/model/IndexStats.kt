package id.ont.know.model

data class IndexStats(val distinct_terms: Int,
                      val aggregate_mappings: Int,
                      val histogram: Map<String, Map<String, Int>>,
                      val allCounts: LinkedHashMap<Int, MutableSet<String>>

)